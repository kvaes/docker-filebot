#!/bin/bash
#!/bin/bash

LOCK=/tmp/copyflat.lock

if [ ! -f $LOCK ]
then
  echo "no lock, setting lock"
  touch $LOCK
else
  echo "locked, exiting"
  exit 1
fi

filebot \
-script fn:amc \
-extract \
--log-file /var/log/filebot.log \
--action move \
-non-strict \
/input \
--def \
"seriesFormat=/output/Episodes/{n.ascii()}/{'S'}{s.pad(2)}/{n.ascii()} - {s00e00} - {t.ascii()}" \
"movieFormat=/output/Movies/{n.ascii()} [{vf}] [{rating}] ({y})/{n.ascii()} [{vf}] [{rating}] ({y}){' CD'+pi}{'.'+lang}" \
--conflict auto \
--def subtitles=nl,de \
--def artwork=y \
--def reportError=y \
--def clean=y \
--lang en 

echo "unlocking"
rm $LOCK
